<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
  <head>
	<meta charset="UTF-8">
	<title>ClientManager</title>
	<link rel="stylesheet" type="text/css" href="CSS/semantic.min.css">
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="JS/semantic.min.js"></script>
  </head>

  <header style="background-color: teal; height: 70px;">
  	<h2 class="ui header" style="padding: 9px">
  	  <div class="content" style="color: white;">
    	Clients Manager
    	<div class="sub header" style="color: white">Check out our plug-in marketplace</div>
  	  </div>
  	  <div class="content" style="float: right; margin-top: 7px;">
	    <form action="logouted.jsp">
    	  <button class="ui teal button">ログアウト</button>
	    </form>
  	  </div>
	</h2>
  </header>

  <body>
  <div style="margin-left: 33%; margin-top: 50px;" >
  <form action="regist.jsp">
   	<button class="ui massive button">
  		新しい得意先を登録する
	</button>
   </form>
   <form action="view.jsp" style="padding-top: 30px;">
   	  <button class="ui massive button">
  		得意先の一覧を確認する
	  </button>
   </form>
   <form action="viewOrder.jsp" style="padding-top: 30px;">
   	  <button class="ui massive button">
  		得意先の受注を確認する
	  </button>
   </form>
  </div>

  </body>
</html>