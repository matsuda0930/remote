<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
  <head>
	<meta charset="UTF-8">
	<title>認証エラー</title>
	<link rel="stylesheet" type="text/css" href="CSS/semantic.min.css">
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="JS/semantic.min.js"></script>
  </head>
  <header style="background-color: teal; height: 70px;">
  	<h2 class="ui header" style="padding: 9px">
  	  <div class="content" style="color: white;">
    	Clients Manager
    	<div class="sub header" style="color: white">Check out our plug-in marketplace</div>
  	  </div>
	</h2>
  </header>
  <body>
    <div style="margin-left: 25%; width:50%; margin-top: 40px;"class="ui negative message centered">
        <div class="header">
		 	認証に失敗しました
        </div>
      <p>従業員番号とパスワードを確認してください</p>
    </div>
  <form style="width:30%; align: center; margin-top: 50px; margin-left: 33%"
  	class="ui form" action="/LAdev_ver01.00/AuthServlet" method="POST">
  		<div class="field ">
    		<label>ユーザ名</label>
    		<input type="text" name="j_userid" placeholder="UserID" maxlength="4">
  		</div>
  		<div class="field">
    		<label>パスワード</label>
    		<input type="password" name="j_password" placeholder="Password" maxlength="12">
  		</div>
  		<div class="ui grid centered" style="margin-top: 20px;">
  		  <button class="ui button centered" type="submit">ログイン</button>
  		</div>
	</form>
  </body>
</html>