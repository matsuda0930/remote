<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
  <head>
	<meta charset="UTF-8">
	<title>ClientsManager</title>
	<link rel="stylesheet" type="text/css" href="CSS/semantic.min.css">
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="JS/semantic.min.js"></script>
  </head>

  <header style="background-color: teal; height: 70px;">
  	<h2 class="ui header" style="padding: 9px">
  	  <div class="content" style="color: white;">
    	Clients Manager
    	<div class="sub header" style="color: white">Check out our plug-in marketplace</div>
  	  </div>
  	  <div class="content" style="float: right; margin-top: 7px;">
	    <form action="logouted.jsp">
    	  <button class="ui teal button">ログアウト</button>
	    </form>
  	  </div>
	</h2>
  </header>

  <body>
    <div class="ui header centered">
      <h2>登録内容を入力してください</h2>
    </div>
  <form style="width:40%; align: center; margin-top: 20px; margin-left: 30%"
  	class="ui form" action="regist.jsp" method="POST">
  <label for="fruit">法人顧客か個人顧客か選択してください</label>
  <div class="inline fields centered" style="padding-top: 10px;">
    <div class="field">
      <div class="ui radio checkbox">
        <input type="radio" name="clientType"  tabindex="0">
        <label>法人</label>
      </div>
    </div>
    <div class="field">
      <div class="ui radio checkbox">
        <input type="radio" name="clientType" tabindex="0">
        <label>個人</label>
      </div>
    </div>
  </div>

  	  <label>得意先コード</label>
  		<div class="field ">
    		<input type="text" name="clientcode" placeholder="clientcode"
    			required="required" maxlength="6">
  		</div>

  	  <label>得意先顧客名</label>
  	    <div class="field ">
    		<input type="text" name="clientname" placeholder="clientname" required="required">
  		</div>

  	  <label>フリガナ</label>
  	    <div class="field ">
    		<input type="text" name="phonitic" placeholder="ケンコウショクヒン" required="required">
  		</div>

  	  <label>電話番号（ハイフン有りで登録してください）</label>
  		<div class="field ">
    		<input type="text" name="clienttel" placeholder="00-0000-0000" required="required">
  		</div>

      <label>郵便番号（ハイフン有りで登録してください）</label>
  		<div class="field ">
    		<input type="text" name="clientpost" placeholder="000-0000" required="required">
  		</div>

	  <label>住所１</label>
  		<div class="field ">
    		<input type="text" name="clientaddress1" placeholder="東京都新宿区新宿５－１１－２" required="required">
  		</div>

  	  <label>住所２</label>
  		<div class="field ">
    		<input type="text" name="clientaddress2" placeholder="SOBIZGATES２階">
  		</div>

  	  <div class="inline fields" style="margin-left: 28%; padding-top: 10px">
  		<div class="field">
 	      <button class="ui green button" type="submit">登録</button>
  		</div>
  		<input type="hidden" name="action" value="regist">
  	  </form>
  		<div class="field">
  		   <form action="top.jsp">
  		      <button class="ui button" type="submit">キャンセル</button>
  		   </form>
  		</div>
      </div>
  </body>
</html>