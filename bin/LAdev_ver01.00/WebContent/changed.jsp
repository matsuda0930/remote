<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
  <head>
	<meta charset="UTF-8">
	<title>ClientsManager</title>
	<link rel="stylesheet" type="text/css" href="CSS/semantic.min.css">
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script src="JS/semantic.min.js"></script>
  </head>

  <header style="background-color: teal; height: 70px;">
  	<h2 class="ui header" style="padding: 9px">
  	  <div class="content" style="color: white;">
    	Clients Manager
    	<div class="sub header" style="color: white">Check out our plug-in marketplace</div>
  	  </div>
  	  <div class="content" style="float: right; margin-top: 7px;">
	    <form action="logouted.jsp">
    	  <button class="ui teal button">ログアウト</button>
	    </form>
  	  </div>
	</h2>
  </header>

  <body>
    <div style="margin-left: 25%; width:50%; margin-top: 40px;"class="ui success message">
      <i class="close icon"></i>
        <div class="header">
		 	変更が完了しました
        </div>
      <p>続けて変更するには、「一覧を見る」ボタンを押して、変更する得意先を選んでください</p>
    </div>

    <div class="ui grid centered" style="margin-top: 100px">
  	  <form action="view.jsp" class="three wide column">
  	    <button class="ui button" type="submit">一覧を見る</button>
  	  </form>
    </div>
  </body>
</html>