<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
  <head>
	<meta charset="UTF-8">
	<title>ログイン</title>
	<link rel="stylesheet" type="text/css" href="CSS/semantic.min.css">
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="JS/semantic.min.js"></script>
  </head>
  <header style="background-color: blue;">
  	<h2 class="ui header">
  	<div class="content" style="color: white;">
    	Login
    	<div class="sub header" style="color: white">Check out our plug-in marketplace</div>
  	</div>
	</h2>
  </header>
  <body>
  <form style="width:30%; align: center; margin-top: 50px; margin-left: 33%"
  	class="ui form" action="/LAdev_ver01.00/AuthServlet" method="POST">
  		<div class="field ">
    		<label>ユーザ名</label>
    		<input type="text" name="j_username" placeholder="Username">
  		</div>
  		<div class="field">
    		<label>パスワード</label>
    		<input type="password" name="j_password" placeholder="Password">
  		</div>
  		<button class="ui button" type="submit">ログイン</button>
	</form>
  </body>
</html>