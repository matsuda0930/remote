<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
  <head>
	<meta charset="UTF-8">
	<title>ClientsManager</title>
	<link rel="stylesheet" type="text/css" href="CSS/semantic.min.css">
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="JS/semantic.min.js"></script>
  </head>

  <header style="background-color: teal; height: 70px;">
  	<h2 class="ui header" style="padding: 9px">
  	  <div class="content" style="color: white;">
    	Clients Manager
    	<div class="sub header" style="color: white">Check out our plug-in marketplace</div>
  	  </div>
  	  <div class="content" style="float: right; margin-top: 7px;">
	    <form action="logouted.jsp">
    	  <button class="ui teal button">ログアウト</button>
	    </form>
  	  </div>
	</h2>
  </header>


  <body>
  	<!--delete alert-->
    <div style="margin-left: 25%; width:50%; margin-top: 40px;"class="ui negative message">
      <i class="close icon"></i>
        <div class="header">
		 	以下の内容を削除します
        </div>
      <p>削除を完了するには、削除ボタンを押してください</p>
    </div>

    <div class="ui raised very padded text container segment" style="margin: 30px;">
      <div style="width: 35%; margin-left: 10%; margin-top: 10px; float: left;">
      <div class="content">
        <label>得意先ID</label><hr>
      	<p>000001</p><br>
      </div>
      <div class="content">
      	<label>得意先名</label><hr>
      	<p>水素の音～～～</p><br>
      </div>
      <div class="content">
      	<label>電話番号</label><hr>
      	<p>03-5368-3889</p><br>
      </div>
      <div class="content">
      	<label>郵便番号</label><hr>
      	<p>123-3455</p><br>
      </div>
      <div class="content">
      	<label>住所１</label><hr>
      	<p>東京都新宿区新宿５－１１－２</p><br>
      </div>
      <div class="content">
      	<label>住所２</label><hr>
      	<p>SOBIZGATES ２階</p><br>
      </div>
     </div>
    </div>
    <div class="ui raised very padded text container segment" style="margin: 30px;">
      <div style="width: 35%; margin-left: 10%; margin-top: 10px; float: right;">
      <div class="content">
        <label>得意先ID</label><hr>
      	<p>000001</p><br>
      </div>
      <div class="content">
      	<label>得意先名</label><hr>
      	<p>水素の音～～～</p><br>
      </div>
      <div class="content">
      	<label>電話番号</label><hr>
      	<p>03-5368-3889</p><br>
      </div>
      <div class="content">
      	<label>郵便番号</label><hr>
      	<p>123-3455</p><br>
      </div>
      <div class="content">
      	<label>住所１</label><hr>
      	<p>東京都新宿区新宿５－１１－２</p><br>
      </div>
      <div class="content">
      	<label>住所２</label><hr>
      	<p>SOBIZGATES ２階</p><br>
      </div>
     </div>
    </div>

    <div class="ui grid centered">
      <form action="deleted.jsp" class="two wide column">
   	    <button class="ui button red" type="submit">削除</button>
   		<input type="hidden" name="action" value="delete">
  	  </form>
  	  <form action="view.jsp" class="two wide column">
  	    <button class="ui button" type="submit">取消</button>
  	  </form>
    </div>
  </body>
</html>

