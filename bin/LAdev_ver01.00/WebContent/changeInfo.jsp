<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
  <head>
	<meta charset="UTF-8">
	<title>ClientsManager</title>
	<link rel="stylesheet" type="text/css" href="CSS/semantic.min.css">
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="JS/semantic.min.js"></script>
  </head>

  <header style="background-color: teal; height: 70px;">
  	<h2 class="ui header" style="padding: 9px">
  	  <div class="content" style="color: white;">
    	Clients Manager
    	<div class="sub header" style="color: white">Check out our plug-in marketplace</div>
  	  </div>
  	  <div class="content" style="float: right; margin-top: 7px;">
	    <form action="logouted.jsp">
    	  <button class="ui teal button">ログアウト</button>
	    </form>
  	  </div>
	</h2>
  </header>


  <body>
  	<!--delete alert-->
    <div style="margin-left: 30%; width:40%; margin-top: 20px;"class="ui teal message">
      <i class="close icon"></i>
        <div class="header">
		 	以下の内容を変更します
        </div>
      <p>変更項目を入力し、変更ボタンをクリックしてください</p>
    </div>

    <form style="width:40%; align: center; margin-top: 20px; margin-left: 30%"
  	class="ui form" action="changed.jsp" method="POST">
  	  <label>得意先顧客名</label>
  	    <div class="field ">
    		<input type="text" name="clientname" placeholder="clientname" required="required">
  		</div>

  	  <label>フリガナ</label>
  	    <div class="field ">
    		<input type="text" name="phonitic" placeholder="ケンコウショクヒン" required="required">
  		</div>

  	  <label>電話番号（ハイフン有りで登録してください）</label>
  		<div class="field ">
    		<input type="text" name="clienttel" placeholder="00-0000-0000" required="required">
  		</div>

      <label>郵便番号（ハイフン有りで登録してください）</label>
  		<div class="field ">
    		<input type="text" name="clientpost" placeholder="000-0000" required="required">
  		</div>

	  <label>住所１</label>
  		<div class="field ">
    		<input type="text" name="clientaddress1" placeholder="東京都新宿区新宿５－１１－２" required="required">
  		</div>

  	  <label>住所２</label>
  		<div class="field ">
    		<input type="text" name="clientaddress2" placeholder="SOBIZGATES２階">
  		</div>


  		<div class="inline fields" style="margin-left: 33%; padding-top: 10px">
  		  <div class="field">
 	        <button class="ui teal button" type="submit">変更</button>

  		  </div>
  		</form>
  		  <div class="field">
  		  	<form action="view.jsp">
  		      <button class="ui button" type="submit">取消</button>
  		    </form>
  		  </div>
  		</div>
  </body>
</html>

