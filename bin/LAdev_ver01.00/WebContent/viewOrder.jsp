<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
  <head>
	<meta charset="UTF-8">
	<title>ClientsManager</title>
	<link rel="stylesheet" type="text/css" href="CSS/semantic.min.css">
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script src="JS/semantic.min.js"></script>
  </head>

  <header style="background-color: teal; height: 70px;">
  	<h2 class="ui header" style="padding: 9px">
  	  <div class="content" style="color: white;">
    	Clients Manager
    	<div class="sub header" style="color: white">Check out our plug-in marketplace</div>
  	  </div>
  	  <div class="content" style="float: right; margin-top: 7px;">
	    <form action="logouted.jsp">
    	  <button class="ui teal button ">ログアウト</button>
	    </form>
  	  </div>
	</h2>
  </header>

  <body>
  	<div class="ui grid centered" style="width: 60%; margin-left: 20%; margin-top: 30px; margin-bottom: 30px;">
  	  <form class="ui form" action="/ClientsServlet" style="margin-right: 6px">
  	      <input style="width: 350px; float: left;" placeholder="得意先の名前">
  	      <button style="float: right;" class="ui button" type="submit">
  	               名前で絞り込み検索
  	      </button>
  	  </form>
  	  <form class="ui form"  action="/ClientsServlet">
  	      <input type="date"  style="width: 350px; float: left;" placeholder="2019/06/12">
  	      <button style="float: right; margin-right: 10px;" class="ui button" type="submit">
  	                日付で絞り込み検索
  	      </button>
  	  </form>
  	</div>
	  <table class="ui unstackable table" style="width: 96%; margin-left: 2%;">
		<tr>
		  <th>日付</th>
		  <th>得意先名</th>
		  <th>商品名</th>
		  <th>受注数</th>
		</tr>
	<!--<c:forEach items="${clients}" var="client">
		<tr>
			<td>${client.code}</td>
			<td>${client.name}</td>
			<td>${client.tel}</td>
			<td>${client.postal}</td>
			<td>${client.address1}</td>
			<td>${client.address2}</td>
		</tr>
	</c:forEach>-->
		<tr>
		  <td class="date">2019/06/12</td>
		  <td>水素の音～～～</td>
		  <td>水素水</td>
		  <td>1000000/本</td>
		</tr>

		<tr>
		  <td class="date">2019/06/12</td>
		  <td>水素の音～～～</td>
		  <td>水素水</td>
		  <td>1000000/本</td>
		</tr>


	</table>
	<form action="top.jsp">
	  <button style="margin-left: 40%;"class="ui button" type="submit">Topに戻る</button>
	</form>
  </body>
</html>