<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<head>
<link rel="stylesheet" type="text/css" href="CSS/semantic.min.css">
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  ></script>
<script src="JS/semantic.min.js"></script>
</head>


ソート：<a href="/lesson/ItemServlet2?action=sort&key=price_asc"
	class="ui two column compact menu center aligned grid">値段の低い順</a> ,
<a href="/lesson/ItemServlet2?action=sort&key=price_desc"
class="ui two column compact menu center aligned grid">値段の高い順</a><br>

<form class="ui form" action="/lesson/ItemServlet2" method="post">
	<div class="three fields">
		<div class="field">
			<label>商品名</label>
			<input type="text" name="name" placeholder="商品名を入力してください">
		</div>
		<div class="field">
			<label>価格</label>
			<input type="text" name="price" size="5" placeholder="値段を入力してください">
		</div>
		<div class="field">
			<input type="submit" class="ui secondary button" value="追加">
		</div>
		<input type="hidden" name="action" value="add">
	</div>
</form>

<form class="ui form" action="/lesson/ItemServlet2" method="post">
		<div class="three fields">
			<div class="field">
				<label>検索</label>
				<input type="text" name="price" size="5" placeholder="値段を入力してください">
			</div>
			<div class="field">
				<input class="ui secondary button" type="submit" value="検索">
			</div>
		</div>
<input type="hidden" name="action" value="search">
</form>

<form class="ui form" action="/lesson/ItemServlet2" method="post">
		<div class="four fields">
			<div class="field">
				<label>商品番号</label>
				<input type="text" name="code" size="5" placeholder="商品番号を入力してください">
			</div>
			<div class="field">
				<input class="ui button" type="submit" value="削除">
			</div>
		</div>
	<input type="hidden" name="action" value="delete">
</form>


<form class="ui form" action="/lesson/ItemServlet2" method="post">
		<div class="four fields">
			<div class="field">
				<label>商品番号</label>
				<input type="text" name="code" size="5" placeholder="商品番号を入力してください">
			</div>
			<div class="field">
				<label>値段</label>
				<input type="text" name="price" size="5" placeholder="商品番号を入力してください">
			</div>
		</div>
<input class="ui button" type="submit" value="変更">
<input type="hidden" name="action" value="priceChange">
</form>

