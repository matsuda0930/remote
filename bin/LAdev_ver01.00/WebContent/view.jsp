<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
  <head>
	<meta charset="UTF-8">
	<title>ClientsManager</title>
	<link rel="stylesheet" type="text/css" href="CSS/semantic.min.css">
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script src="JS/semantic.min.js"></script>
  </head>

  <header style="background-color: teal; height: 70px;">
  	<h2 class="ui header" style="padding: 9px">
  	  <div class="content" style="color: white;">
    	Clients Manager
    	<div class="sub header" style="color: white">Check out our plug-in marketplace</div>
  	  </div>
  	  <div class="content" style="float: right; margin-top: 7px;">
	    <form action="logouted.jsp">
    	  <button class="ui teal button ">ログアウト</button>
	    </form>
  	  </div>
	</h2>
  </header>

  <body>
  	<div class="ui grid centered" style="width: 60%; margin-left: 20%">
  	  <form class="ui form"
  	    style="padding: 20px;" action="/ClientsServlet">
  	      <input style="width: 350px; margin-left: 20px; float: left;" placeholder="得意先の名前">
  	      <button style="float: right;margin-right: 10px;" class="ui button" type="submit">
  	               名前で絞り込み検索
  	      </button>
  	  </form>
  	  <form class="ui form"
  	    style="float: left; padding-right:20px; padding-bottom:20px;" action="/ClientsServlet">
  	      <input style="width: 350px; margin-left: 20px;" placeholder="得意先コード">
  	      <button style="float: right; margin-right: 10px;" class="ui button" type="submit">
  	 		得意先コードで絞り込み検索
  	      </button>
  	  </form>
  	</div>
	  <table class="ui unstackable table" style="width: 96%; margin-left: 2%;">
		<tr>
		  <th>得意先ID</th>
		  <th>得意先名</th>
		  <th>電話番号</th>
		  <th>郵便番号</th>
		  <th>住所１</th>
		  <th>住所２</th>
		  <th></th>
		  <th></th>
		</tr>
	<!--<c:forEach items="${clients}" var="client">
		<tr>
			<td>${client.code}</td>
			<td>${client.name}</td>
			<td>${client.tel}</td>
			<td>${client.postal}</td>
			<td>${client.address1}</td>
			<td>${client.address2}</td>
		</tr>
	</c:forEach>-->
		<tr>
		  <td>000000</td>
		  <td>水素の音～～～</td>
		  <td>0120-111-128</td>
	      <td>123-4567</td>
		  <td>東京都新宿区歌舞伎町１-２-８</td>
		  <td>H20ビルディング11階</td>
		  <td>
		    <form action="changeInfo.jsp">
		      <button class="ui blue button">変更</button>
		    </form>
		  </td>
		  <td>
		    <form action="delete.jsp">
		      <button class="ui pink button">削除</button>
		    </form>
		  </td>
		</tr>

		<tr>
		  <td>000000</td>
		  <td>根っこ</td>
		  <td>0120-345-7678</td>
	      <td>123-4567</td>
		  <td>東京都新宿区歌舞伎町１１-８</td>
		  <td></td>
		  <td>
		    <form action="changeInfo.jsp">
		      <button class="ui blue button">変更</button>
		    </form>
		  </td>
		  <td>
		    <form action="delete.jsp">
		      <button class="ui pink button">削除</button>
		    </form>
		  </td>
		</tr>
	</table>
	<form action="top.jsp">
	  <button style="margin-left: 40%;"class="ui button" type="submit">Topに戻る</button>
	</form>
  </body>
</html>